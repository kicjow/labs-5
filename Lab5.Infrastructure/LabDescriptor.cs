﻿using System;
using System.Reflection;
using Glowny.Kontrakt;
using Glowny.Implementacja;
using Wyswietlacz.Implementacja;
using Wyswietlacz.Kontrakt;
using PK.Container;
using MojKontener;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(MojContainer);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IWindaKosmiczna));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(WindaKosmiczna));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietlacz.Implementacja.Wyswietlacz));

        #endregion
    }
}
