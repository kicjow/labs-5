﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;
using System.Reflection;

namespace MojKontener
{
    public class MojContainer : IContainer
    {



        private IDictionary<Type, Type> inst1;
        private IDictionary<Type, object> inst2;
        private IDictionary<Type, Delegate> inst3;

       
        public void Register(Assembly assembly)
        {
            foreach (Type item in assembly.GetExportedTypes())
            {
                Register(item);
            }
        }

        public MojContainer()
       {
           this.inst1 =  new Dictionary<Type, Type>();
           this.inst2 = new Dictionary<Type, object>();
           this.inst3 = new Dictionary<Type, Delegate>();
       }
        
       
        public void Register<T>(T impl) where T : class
        {
            Register(typeof(T), impl);

        }
        
        public void Register(Type type, object impl)
        {
            if (type.IsInterface)
            {
                
                if (inst2.ContainsKey(type))
                {
                    inst2[type] = impl;
                }
                else
                {
                    inst2.Add(type, impl);
                }
            }
            else
            {
                foreach (Type item in type.GetInterfaces())
                {
                    if (inst1.Any(x => x.Key.Equals(item)))
                    {
                        inst2[item] = impl;
                    }
                    else
                    {
                        inst2.Add(item, impl);
                    }
                }
            }
            
        }

        
        public void Register<T>(Func<T> provider) where T : class
        {

            if (typeof(T).IsInterface)
            {
                if (inst3.ContainsKey(typeof(T)))
                {
                  
                    inst3[typeof(T)] = provider;
                }
                else
                {
                    inst3.Add(typeof(T), provider);
                }
            }
            else
            {
                foreach (var item in typeof(T).GetInterfaces())
                {
                    if (inst3.ContainsKey(item))
                    {

                        inst3[item] = provider;
                    }
                    else
                    {
                        
                        inst3.Add(item, provider);
                    }
                }
            }
        }
      
        public T Resolve<T>() where T : class
        {
            return Resolve(typeof(T)) as T;
        }

        public void Register(Type type)
        {

            foreach (var item in type.GetInterfaces())
            {
                if (inst1.Any(x => x.Key.Equals(item)))
                {
                    inst1[item] = type;
                }
                else
                {
                    inst1.Add(item, type);
                }
            }
        }


        
        public object Resolve(Type type)
        {

            if (inst3.ContainsKey(type))
            {
                return inst3[type].DynamicInvoke();
            }
          
            if (inst1.ContainsKey(type))
            {
                ICollection<object> parametry = new List<object>();
                int licznik = 0;
                object implemetacja = null;

                foreach (ConstructorInfo konst in inst1[type].GetConstructors())
                {
                    ParameterInfo[] param = konst.GetParameters();
                    if (param.Count() > licznik)
                    {
                        licznik = param.Count(x => x.ParameterType.IsInterface);
                        parametry.Clear();
                        implemetacja = null;

                        foreach (var parametr in param)
                        {
                            if (parametr.ParameterType.IsInterface)
                            {
                                implemetacja = Resolve(parametr.ParameterType);
                            }
                            if (implemetacja == null)
                            {
                                throw new UnresolvedDependenciesException();
                            }
                            parametry.Add(implemetacja);
                        }
                    }
                }
                return Activator.CreateInstance(inst1[type], parametry.ToArray());
            }

            if (inst2.ContainsKey(type))
            {
                return inst2[type];
            }

            if (!type.IsInterface)
            {
                throw new ArgumentException("nidyrydy");
            }


            return null;
        }
    }
}

//public void Register(object impl)
//{

//    var implementation = impl.GetType().GetInterfaces();

//    if (implementation.Length == 0)
//    {
//        return;
//    }

//    foreach (Type item in implementation)
//    {
//        Register(item, impl);
//    }
//}